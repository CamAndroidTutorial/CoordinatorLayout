package com.bunhann.coordinatorlayout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnFabSnack, btnFabFollow, btnCollaps, btnCustom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnFabSnack = findViewById(R.id.btnFABSnackBar);
        btnFabFollow = findViewById(R.id.btnFABFollowWidget);
        btnCollaps = findViewById(R.id.btnCollaps);
        btnCustom = findViewById(R.id.btnCustom);

        btnCustom.setOnClickListener(this);
        btnFabFollow.setOnClickListener(this);
        btnCollaps.setOnClickListener(this);
        btnFabSnack.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent i;
        switch (id){
            case R.id.btnFABSnackBar:
                i = new Intent(v.getContext(), FabAndSnackbarActivity.class);
                startActivity(i);
                break;
            case R.id.btnFABFollowWidget:
                i = new Intent(v.getContext(), FabFollowsWidgetActivity.class);
                startActivity(i);
                break;
            case R.id.btnCollaps:
                i = new Intent(v.getContext(), CollapsingToolbarActivity.class);
                startActivity(i);
                break;
            case R.id.btnCustom:
                i = new Intent(v.getContext(), CustomBehaviorActivity.class);
                startActivity(i);
                break;
            default:
                return;
        }
    }
}
